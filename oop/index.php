<?php 

require_once('Animal.php');
require_once('Ape.php');
require_once('Frog.php');


$hewan = new Animal("shaun");

echo "Name: $hewan->name. <br>";
echo "Legs: $hewan->legs. <br>";
echo "Cold blooded: $hewan->cold_blooded. <br><br>";

$hewan2 = new Ape("Kera Sakti");

echo "Name: $hewan2->name.<br>";
echo "Legs: $hewan2->legs.<br>";
echo "Cold blooded: $hewan2->cold_blooded.<br>";
echo $hewan2->yell();

$hewan3 = new Frog("buduk");

echo "Name: $hewan3->name.<br>";
echo "Legs: $hewan3->legs.<br>";
echo "Cold blooded: $hewan3->cold_blooded.<br>";
echo $hewan3->jump();